/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.nmea.operator.network.decoder;

import io.scenarium.flow.EvolvedOperator;
import io.scenarium.flow.ParamInfo;

import net.sf.marineapi.nmea.parser.SentenceFactory;
import net.sf.marineapi.nmea.sentence.Sentence;
import net.sf.marineapi.nmea.sentence.SentenceValidator;

public class NMEA0183Decoder extends EvolvedOperator {
	private boolean isWarning = false;

	@Override
	public void birth() throws Exception {}

	@Override
	public void death() throws Exception {}

	@ParamInfo(out = "Out")
	public Sentence process(String data) {
		if (SentenceValidator.isValid(data)) {
			if (this.isWarning)
				setWarning(null);
			return SentenceFactory.getInstance().createParser(data);
		}
		setWarning("Invalid NMEA sentence: " + data);
		this.isWarning = true;
		return null;
	}
}
