/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.nmea.editor;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import io.beanmanager.editors.PropertyEditor;

import net.sf.marineapi.nmea.parser.SentenceFactory;
import net.sf.marineapi.nmea.sentence.Sentence;

public class NMEA0183Editor extends PropertyEditor<Sentence> {
	@Override
	public String getAsText() {
		Object val = getValue();
		return val == null ? String.valueOf((Object) null) : val.toString();
	}

	@Override
	public boolean hasCustomEditor() {
		return false;
	}

	@Override
	public Sentence readValue(DataInput raf) throws IOException {
		return SentenceFactory.getInstance().createParser(raf.readUTF());
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (text.equals(String.valueOf((Object) null)))
			setValue(null);
		setValue(SentenceFactory.getInstance().createParser(text));
	}

	@Override
	public void writeValue(DataOutput raf, Sentence value) throws IOException {
		raf.writeUTF(value.toString());
	}
}
