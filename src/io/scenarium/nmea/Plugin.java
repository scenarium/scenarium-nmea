package io.scenarium.nmea;

import io.beanmanager.PluginsBeanSupplier;
import io.beanmanager.consumer.ClassNameRedirectionConsumer;
import io.beanmanager.consumer.EditorConsumer;
import io.scenarium.flow.consumer.ClonerConsumer;
import io.scenarium.flow.consumer.OperatorConsumer;
import io.scenarium.flow.consumer.PluginsFlowSupplier;
import io.scenarium.gui.core.PluginsGuiCoreSupplier;
import io.scenarium.gui.core.consumer.DrawersConsumer;
import io.scenarium.nmea.drawer.GPSDrawer;
import io.scenarium.nmea.editor.NMEA0183Editor;
import io.scenarium.nmea.operator.network.decoder.NMEA0183Decoder;
import io.scenarium.pluginManager.PluginsSupplier;

import net.sf.marineapi.nmea.parser.SentenceFactory;
import net.sf.marineapi.nmea.sentence.Sentence;

public class Plugin implements PluginsSupplier, PluginsFlowSupplier, PluginsBeanSupplier, PluginsGuiCoreSupplier {
	@Override
	public void populateOperators(OperatorConsumer operatorConsumer) {
		operatorConsumer.accept(NMEA0183Decoder.class);
	}

	@Override
	public void populateCloners(ClonerConsumer clonerConsumer) {
		clonerConsumer.accept(Sentence.class, o -> SentenceFactory.getInstance().createParser(o.toString()));
	}

	@Override
	public void populateClassNameRedirection(ClassNameRedirectionConsumer classNameRedirectionConsumer) {
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.network.decoder.NMEA0183Decoder", "io.scenarium.nmea.operator.network.decoder.NMEA0183Decoder");
	}

	@Override
	public void populateEditors(EditorConsumer editorConsumer) {
		editorConsumer.accept(Sentence.class, NMEA0183Editor.class);
	}

	@Override
	public void populateDrawers(DrawersConsumer drawersConsumer) {
		drawersConsumer.accept(Sentence.class, GPSDrawer.class);
	}

}
