import io.scenarium.nmea.Plugin;
import io.scenarium.pluginManager.PluginsSupplier;

open module io.scenarium.nmea {
	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	requires transitive marineapi;
	requires transitive io.scenarium.geographic;

	exports io.scenarium.nmea;
	exports io.scenarium.nmea.editor;
	exports io.scenarium.nmea.operator.network.decoder;

}
